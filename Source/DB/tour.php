<?php

/**
 * Created by PhpStorm.
 * User: Benjamin
 * Date: 03/05/2016
 * Time: 17:28
 */
class tour extends database
{
    /**
     * tournois constructor: connection a la base de donnée avec constructeur de dbhelp
     * usage: $tour = new tour(); car le nom de la classe est tour
     **/
    function __construct()
    {
        parent::__construct();
    }

    /** verifie si un joueur n'est pas déjà inscrit et à réussi à contourner le système
     * @param $pseudo
     * @return bool
     */
    function testJoueur($pseudo){
        $sql="SELECT id FROM joueur_tournois WHERE UPPER(pseudo)=UPPER('$pseudo')";
        $res=$this->query($sql);
        if(pg_num_rows($res) == 1){
            if ($row = pg_fetch_row($res)) {
                return false;
            }
        }
        return true;
    }

    /** ajoute un joueur a la liste des inscrits
     * @param $pseudo
     * @param $elo
     * @return bool
     */
    function addJoueur($pseudo, $elo){
        $sql = "INSERT INTO joueur_tournois (pseudo, nb_elo) VALUES ('$pseudo','$elo')";
        $res = $this->query($sql);
        if (!$res) {
            /*erreur*/
        }
        return true;
    }

    /** retourne le nombre de joueurs inscrits
     * @return bool
     */
    function taille(){
        $sql="SELECT COUNT(id) FROM joueur_tournois";
        $res=$this->query($sql);
        if(pg_num_rows($res) == 1){
            if ($row = pg_fetch_row($res)) {
                return $row[0];
            }
        }
        return false;
    }

    /** récupère les données dans la table config
     * @param $num
     * @param $col
     * @return mixed
     */
    function ligne($num, $col)
    {
        $sql = "SELECT bool,config.int  FROM config WHERE id='$num'";
        $res = $this->query($sql);
        $row = pg_fetch_row($res); //manque les cas d'erreurs si on a pas réussi à récuperer la donnée
        return $row[$col];
    }

    /** remplace les valeur du tournoi dans la table config
     * @param $taille
     */
    function newTour($taille){
        $sql = "UPDATE config SET bool = 1 where id = 1";
        $sql2 = "UPDATE config SET config.int = '$taille' where id = 1";
        $res2=$this->query($sql2);
        $res=$this->query($sql);
    }


    /** supprime un tournoi en plaçant la veleur de bool à 0
     *
     */
    function delTour(){
        $sql = "UPDATE config SET bool = 0 where id = 1";
        $res=$this->query($sql);
    }

    /**usage: liste des inscrits dans un tournois
     * @return array|bool
     */
    function listeInscrit_tournois(){
        $sql = "SELECT pseudo, nb_elo FROM joueur_tournois";
        $res = $this->query($sql);
        if(!$res){
            return False;
        }
        $return = [];
        while(($row = pg_fetch_row($res))){
            $return[$row[0]] = $row[1]; 
        }
        return $return;
    }
}