<?php

/**
 * Created by PhpStorm.
 * User: Benjamin
 * Date: 05/04/2016
 * Time: 15:14
 * Usage: classe pour la connection à la BDD avec pleins de fonctions utiles
 */
class database{

    /**info de connexion à la BDD*/
    private $name = 'babIIE',
        $host = 'localhost',
        $port = 5432,
        $user = 'postgres',
        $password = 'projettopsecret',
        $db;

    /**
     * database constructor: à utiliser pour établir la connection à la BDD
     * usage: $db = new database()
     */
    function __construct()
    {
        $this->db = pg_connect("host=$this->host port=$this->port dbname=$this->name user=$this->user password=$this->password");
        if(!$this->db){
            /*erreur*/
        }
    }

    /**usage: execute la requete $sql a la BDD crée auparavant
     * @param $sql
     * @return resource
     */
    function query($sql){
        return pg_query($this->db,$sql);
    }

    /**usage: classement des $limit premier joueurs décalé de $offset (utile si le nombre des joueurs est grand et
     * qu'on veut les affichers par "paquet" de 20 par exemple
     * @return array|bool
     */
    function Classement($limit,$offset){
        $sql = "SELECT pseudo, nb_elo FROM joueur ORDER BY nb_elo DESC LIMIT '$limit' OFFSET '$offset'";
        $res = $this->query($sql);
        if(!$res){
            return False;
        }
        $return = [];
        while(($row = pg_fetch_row($res))){
            $return[$row[0]] = $row[1];
        }
        return $return;
    }
    
    /**usage: recupère le nom en fonction du pseudo
     * @param $pseudo
     * @return mixed
     */
    function getNom($pseudo){
        $sql = "SELECT nom from joueur where pseudo='$pseudo'";
        $res=$this->query($sql);
        $row=pg_fetch_row($res); //manque les cas d'erreurs si on a pas réussi à récuperer la donnée
        return $row[0];
    }
    /**usage: recupère le prenom en fonction du pseudo
     * @param $pseudo
     * @return mixed
     */
    function getPrenom($pseudo){
        $sql = "SELECT prenom from joueur where pseudo='$pseudo'";
        $res=$this->query($sql);
        $row=pg_fetch_row($res); //manque les cas d'erreurs si on a pas réussi à récuperer la donnée
        return $row[0];
    }

    /** usage: recupère le mail en fonction du pseudo
     * @param $pseudo
     * @return mixed
     */
    function getEmail($pseudo){
        $sql = "SELECT email from joueur where pseudo='$pseudo'";
        $res=$this->query($sql);
        $row=pg_fetch_row($res); //manque les cas d'erreurs si on a pas réussi à récuperer la donnée
        return $row[0];
    }

    /** usage: recupère la date d'inscription en fonction du pseudo
     * @param $pseudo
     * @return mixed
     */
    function getDate($pseudo){
        $sql = "SELECT date_ins from joueur where pseudo='$pseudo'";
        $res=$this->query($sql);
        $row=pg_fetch_row($res); //manque les cas d'erreurs si on a pas réussi à récuperer la donnée
        return $row[0];
    }

    /** usage: recupère le nb_elo en fonction du pseudo
     * @param $pseudo
     * @return bool
     */
    function getElo($pseudo){
        $sql = "SELECT nb_elo FROM joueur WHERE pseudo ='$pseudo'";
        $res = $this->query($sql);
        if($row = pg_fetch_row($res)){
            if(pg_num_rows($res)==1){
                return $row[0];
            }
        }
        return false;
    }

    /** usage: recupère admin en fonction du pseudo
     * @param $pseudo
     * @return mixed
     */
    function getAdmin($pseudo){
        $sql = "SELECT admin from joueur where pseudo='$pseudo'";
        $res=$this->query($sql);
        $row=pg_fetch_row($res); //manque les cas d'erreurs si on a pas réussi à récuperer la donnée
        return $row[0];
    }

    /** recupere la position du joueur dans le classement
     * @param $pseudo
     * @return bool|mixed
     */
    function getClassement($pseudo){
        $sql = "SELECT pseudo FROM joueur ORDER BY nb_elo DESC";
        $res = $this->query($sql);
        if(!$res){
            return False;
        }
        $return = [];
        $i = 1;
        while(($row = pg_fetch_row($res))){
            $return[$row[0]] = $i++; 
        }
        return $return[$pseudo];
    }

    /** retourne la taille de la table joueur
     * @return bool
     */
    function tailleJoueur(){
        $sql="SELECT COUNT(pseudo) FROM joueur";
        $res=$this->query($sql);
        if(pg_num_rows($res) == 1){
            if ($row = pg_fetch_row($res)) {
                return $row[0];
            }
        }
        return false;
    }

    /** retourne le nombre de nouveaux utilisateurs journaliers
     * @return bool
     */
    function newUser(){
        $sql = "SELECT COUNT(pseudo) FROM joueur WHERE date_ins = CURRENT_DATE";
        $res=$this->query($sql);
        if(pg_num_rows($res) == 1){
            if ($row = pg_fetch_row($res)) {
                return $row[0];
            }
        }
        return false;
    }

    /** retourne le nombre de nouveaux matches journaliers
     * @return bool
     */
    function newMatch(){
        $sql = "SELECT COUNT(id) FROM rencontre WHERE date = CURRENT_DATE";
        $res=$this->query($sql);
        if(pg_num_rows($res) == 1){
            if ($row = pg_fetch_row($res)) {
                return $row[0];
            }
        }
        return false;
    }
    /** retourne la taille de la table rencontre
     * @return bool
     */
    function tailleRencontre(){
        $sql="SELECT COUNT(id) FROM rencontre";
        $res=$this->query($sql);
        if(pg_num_rows($res) == 1){
            if ($row = pg_fetch_row($res)) {
                return $row[0];
            }
        }
        return false;
    }
}




