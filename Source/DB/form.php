<?php
/**
 * Created by PhpStorm.
 * User: Benjamin
 * Date: 06/04/2016
 * Time: 21:21
 * Usage: classe qui traite le cas d'authentification et tout donc seulement utile pour la page d'accueil
 */
class form extends database
{
    /**
     * loginform constructor: connection a la base de donnée avec constructeur de dbhelp
     * usage: $form = new form(); car le nom de la classe est form
     */
    function __construct()
    {
        parent::__construct();
    }

    /**usage: teste si la table joueur est vide (pas encore et peut etre jamais utile)
     * @return bool
     */
    function estVide()
    {
        $sql = "SELECT * FROM joueur";
        $res = $this->query($sql);
        return (pg_fetch_row($res) == 0);
    }

    /** @param $nom,$pseudo,$prenom,$mail,$mdp
     * ajoute a la table joueur un nouveau client
     * @return bool
     */
    function setClient($nom,$pseudo,$prenom,$mail,$mdp)
    {
        $mdp = $this->setHash($mdp);
        $sql = "INSERT INTO joueur (nom, pseudo, prenom, email,pass) VALUES ('$nom','$pseudo','$prenom','$mail','$mdp')";
        $res = $this->query($sql);
        if (!$res) {
            /*erreur*/
        }
        return true;
    }

    /** fonction qui enlève tous les caractères malveilants et interdits d'un texte (à ajouter enfin utiliser!!)
     * @param $data
     * @return string
     */
    function verifInput($data)
    {
        $data = htmlspecialchars(stripslashes(trim($data)));
        return $data;

    }

    /**usage: hash un mot de passe pour ensuite pouvoir l'insérer dans la base
     * @return bool|string
     */
    function setHash($mdp){
        return password_hash($mdp,PASSWORD_BCRYPT);
    }

    /**usage: teste si le mdp entré par l'utilisateur correspond à celui dans la base
     * @param $mdp
     * @param $mdp2
     * @return bool
     */
    function testPass($mdp, $mdp2){
        return password_verify($mdp,$mdp2);
    }

    /**usage: test si le mail/mdp est dans la base pour "se connecter"!!!
     * @param $mail
     * @param $mdp
     * @return bool
     */
    function next($mail, $mdp){
        $sql = "SELECT pass FROM joueur WHERE email='$mail'";
        $res = $this->query($sql);
        if (pg_num_rows($res) == 1){
            if ($row = pg_fetch_row($res)){
                if($this->testPass($mdp,$row[0])) {
                    return True;
                }
            }
        }
        return false;
    }

    /**usage: récupère le pseudo en fonction du mail
     * @param $mail
     * @return bool
     */
    function getPseudo($mail){
        $sql = "SELECT pseudo FROM joueur WHERE email='$mail'";
        $res = $this->query($sql);
        if (pg_num_rows($res) == 1){
            if ($row = pg_fetch_row($res)){
                return $row[0];
            }
        }
        return false;
    }

    /**usage: test si un mail est déjà dans la BDD
     * @param $mail
     * @return bool
     */
    function testMail($mail){
        $sql="SELECT pseudo FROM joueur WHERE email='$mail'";
        $res=$this->query($sql);
        if(pg_num_rows($res) == 1){
            if ($row = pg_fetch_row($res)) {
                return false;
            }
        }
        return true;
    }

    /**usage: test si un pseudo est déjà dans la BDD
     * @param $pseudo
     * @return bool
     */
    function testPseudo($pseudo){
        $sql="SELECT prenom FROM joueur WHERE UPPER(pseudo)=UPPER('$pseudo')";
        $res=$this->query($sql);
        if(pg_num_rows($res) == 1){
            if ($row = pg_fetch_row($res)) {
                return false;
            }
        }
        return true;
    }

    /** update du nom du joueur
     * @param $nom
     * @param $pseudo
     * @return bool
     */
    function setNom($nom, $pseudo){
        $sql = "UPDATE joueur SET nom = '$nom' WHERE pseudo = '$pseudo'";
        $res = $this->query($sql);
        if($res){
            return true;
        }
        return false;

    }
    /** update du prenom du joueur
     * @param $prenom
     * @param $pseudo
     * @return bool
     */
    function setPrenom($prenom,$pseudo){
        $sql = "UPDATE joueur SET prenom = '$prenom' WHERE pseudo = '$pseudo'";
        $res = $this->query($sql);
        if($res){
            return true;
        }
        return false;

    }
    /** update du mail du joueur
     * @param $mail
     * @param $pseudo
     * @return bool
     */
    function setMail($mail,$pseudo){
        $sql = "UPDATE joueur SET email = '$mail' WHERE pseudo = '$pseudo'";
        $res = $this->query($sql);
        if($res){
            return true;
        }
        return false;

    }
    /** update du mot de passe du joueur
     * @param $pass
     * @param $pseudo
     * @return bool
     */
    function setPass($pass,$pseudo){
        $pass = $this->setHash($pass);
        $sql = "UPDATE joueur SET pass = '$pass' WHERE pseudo = '$pseudo'";
        $res = $this->query($sql);
        if($res){
            return true;
        }
        return false;

    }
}