<?php
if(isset($_GET['type'])){$type = $_GET['type'];};
session_start();
/**
 * renvoie le Pseudo de l'user courant pour la bar de nav
 */
function user(){
    echo $_SESSION['user_pseudo']; //recupere le pseudo de la session
}
include 'DB/dbhelp.php';
include 'DB/tour.php';
include 'html/info_user.php';
include 'html/test_login.php';
include 'config.php';
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>BabIIE</title>
    <!-- Bootstrap Core CSS -->
    <link href="styles/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="styles/style-site.css" rel="stylesheet">
    <!-- Morris Charts CSS -->
    <link href="styles/morris.css" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
    <!-- Custom Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link rel="shortcut icon" type="image/png" href="images/ico2.png"/>
</head>

<body>
<div id="wrapper">
    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top top-nav" role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header navbar-left">
            <a href="accueil.php" class="logo text-center"><img src="images/nav.png" alt="Logo de ouf"></a>
            <a class="navbar-brand" href="accueil.php">BabIIE</a>
        </div>
        <!-- Top Menu Items -->
        <ul class="nav navbar-right top-nav">
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="glyphicon glyphicon-user"></i> <?php user();?><span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li>
                        <a href="profil.php"><i class="glyphicon glyphicon-user" style="color: #595a58;"></i> Profile</a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a href="html/logout.php"><i class="glyphicon glyphicon-log-out" style="color: #595a58;"></i> Log Out</a>
                    </li>
                </ul>
            </li>
        </ul>
        <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
        <div class="collapse navbar-collapse navbar-ex1-collapse" id="myNav">
            <ul class="nav navbar-nav side-nav">
                <li>
                    <div class="block padd"><a href="new_match.php"><button type="button" class="btn btn-primary center-block">ADD NEW MATCH</button></a></div>
                </li>
                <?php
                if($_SESSION['user_admin']){
                    echo "<li>
                    <div class=\"block padd\"><a href=\"match_attente.php\"><button type=\"button\" class=\"btn btn-danger center-block\">SEE ALL MATCH</button></a></div>
                </li>";
                }
                ?>
                <li>
                    <a href="accueil.php"><i class="fa fa-fw fa-home"></i>  ACCUEIL</a>
                </li>
                <li>
                    <a href="classement.php"><i class="fa fa-fw fa-list-alt"></i> CLASSEMENT</a>
                </li>
                <li>
                    <a href="tournoi.php"><i class="fa fa-fw fa-sitemap"></i> TOURNOI</a>
                </li>
                <li>
                    <a href="ligue.php"><i class="fa fa-fw fa-soccer-ball-o"></i> LIGUE</a>
                </li>
                <li>
                    <a href="reglement.php"><i class="fa fa-fw fa-cogs"></i> REGLEMENT</a>
                </li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </nav>

    <div id="page-wrapper">

        <div class="container-fluid">

            <!-- Page Heading -->
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">
                        BabIIE <small>Profil</small>
                    </h1>
                </div>
            </div>
            <!-- /.row -->

            <div class="container col-lg-3">
                <div class="thumbnail">
                        <img src="images/wp.png" alt="Wallpaper" style="height: 300px; width: 3000px;">
                    </div>
                <div class="panel panel-baby">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="fa fa-bar-chart-o fa-fw"></i> Informations personnelles</h3>
                    </div>
                    <?php if(!isset($_GET['type']) || $type != 1){}
                    elseif(isset($_GET['type']) && $type == 1){
                        echo "<form action=\"php/edit_profil.php\" method=\"post\">";
                        ;}
                    ?>
                    <div class="panel-body">
                        <dl class="dl-horizontal">
                        <dt>Prénom</dt>
                        <dd><?php if(!isset($_GET['type']) || $type != 1){
                            echo $_SESSION['user_prenom'];}
                            elseif(isset($_GET['type']) && $type == 1){
                                $pre = $_SESSION['user_prenom'];
                            echo "<input type=\"text\" class=\"form-control\" name=\"prenom\" placeholder=\"Prénom\" maxlength=40 value='$pre' required/>"
                            ;}
                            ?></dd>
                        <br>
                        <dt>Nom</dt>
                        <dd><?php if(!isset($_GET['type']) || $type != 1){
                                echo $_SESSION['user_nom'];}
                            elseif(isset($_GET['type']) && $type == 1){
                                $nom = $_SESSION['user_nom'];
                                echo "<input type=\"text\" class=\"form-control\" name=\"nom\" placeholder=\"Nom\" maxlength=40 value='$nom' required>"
                                ;}
                            ?></dd>
                        <br>
                        <dt>Pseudo</dt>
                        <dd><?php echo $_SESSION['user_pseudo'];?></dd>
                        <br>
                        <dt>Classement</dt>
                        <dd><?php echo $_SESSION['user_classement'];?></dd>
                        <br>
                        <dt>Email</dt>
                        <dd><?php if(!isset($_GET['type']) || $type != 1){
                                echo $_SESSION['user_email'];}
                            elseif(isset($_GET['type']) && $type == 1){
                                $mail = $_SESSION['user_email'];
                                echo "	<input type=\"email\" class=\"form-control\" name=\"email\" id=\"email2\" placeholder=\"Email\" maxlength=40 value='$mail' required/>"
                                ;}
                            ?></dd>
                        <br>
                        <dt>Nombre de points</dt>
                        <dd><?php echo $_SESSION['user_elo'];?></dd>
                            <?php if(!isset($_GET['type']) || $type != 1){}
                            elseif(isset($_GET['type']) && $type == 1){
                                echo "<br>
                                        <dt>Password</dt>
                                        <dd>    <input type=\"password\" name=\"pass\" class=\"form-control\" pattern=\".{4,}\" placeholder=\"Password\" maxlength=20 id=\"pass\" title=\"Minimum 4 caractères\" /></dd>
                                        <br>
                                        <dt>Confirm Password</dt>
                            <dd>    <input type=\"password\" name=\"cpass\" class=\"form-control\" pattern=\".{4,}\" placeholder=\"Confirm Password\" maxlength=20 id=\"cpass\" title=\"Minimum 4 caractères\" /></dd>"
                                ;}
                            ?>
                        </dl>
                        <?php if(isset($_GET['type']) && $type == 1){
                            echo "<a href='profil.php?type=1'><button type=\"submit\" class=\"btn btn-success\">Save</button></a> <a href='profil.php'><button type=\"button\" class=\"btn btn-danger\">Cancel</button></a>";

                        }
                        else {
                            echo "<a href='profil.php?type=1'><button type=\"button\" class=\"btn btn-warning\"><i class=\"fa fa-cogs\"></i> Edit</button></a>";
                        }
                        ?>
                        <?php if(!isset($_GET['type']) || $type != 1){}
                        elseif(isset($_GET['type']) && $type == 1){
                            echo "</form>";
                            ;}
                        ?>
                        <?php if(!isset($_GET['type'])){}
                        elseif(isset($_GET['type']) && $type == 2){
                            echo "<h4 class='text-center text-warning'>L'email existe déjà !</h4>";
                            ;}
                        elseif(isset($_GET['type']) && $type == 3){
                            echo "<h4 class='text-center text-warning'>Il faut compléter au moins un champ !</h4>";
                            ;}
                        elseif(isset($_GET['type']) && $type == 4){
                            echo "<h4 class='text-center text-warning'>Mot de passe différents !</h4>";
                            ;}
                        ?>
                    </div>

                </div>
            </div>
            <div class="col-lg-offset-3">
                <h1><?php echo $_SESSION['user_pseudo'];?></h1>
                <blockquote>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</blockquote>
            </div>
            <div class="container col-lg-9">
                <div class="panel panel-baby">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="fa fa-bar-chart-o fa-fw"></i> Statistiques</h3>
                    </div>
                    <br>
                    <h4 class="text-center">Evolution du nombre de points ELO depuis votre inscription !</h4>
                    <div class="panel-body">
                        <div id="area-chart"></div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover ">
                            <?php
                            include 'php/classement_index.php';
                            stat_profil($_SESSION['user_pseudo']);
                            ?>
                        </table>
                    </div>
                </div>
            </div>
            <div class="container col-lg-12">
                <div class="panel panel-baby">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="fa fa-soccer-ball-o fa-fw"></i> Matchs</h3>
                    </div>
                    <div class="panel-body">
                        <h4 class="text-center">Ensemble de vos matchs réalisés !</h4>

                    </div>
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover ">
                            <?php
                            allMatch($_SESSION['user_pseudo']);
                            ?>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /#page-wrapper -->
</div>
<!-- /#wrapper -->
<script src="script/jquery.min.js"></script>

<script>
    // ADD SLIDEDOWN ANIMATION TO DROPDOWN //
    $('.dropdown').on('show.bs.dropdown', function(e){
        $(this).find('.dropdown-menu').first().stop(true, true).slideDown();
    });

    // ADD SLIDEUP ANIMATION TO DROPDOWN //
    $('.dropdown').on('hide.bs.dropdown', function(e){
        $(this).find('.dropdown-menu').first().stop(true, true).slideUp();
    });
</script>
<script src="styles/js/bootstrap.min.js"></script>
<script src="styles/js/morris/raphael.min.js"></script>
<script src="styles/js/morris/morris.min.js"></script>
<script src="styles/js/morris/morris-data.js"></script>
<script>
    Morris.Area({
        element: 'area-chart',
        data: [<?php graph($_SESSION['user_pseudo']);?>
        ],
        xkey: 'date',
        ykeys: 'a',
        ymin: <?php echo minElo($_SESSION['user_pseudo']);?>,
        lineColors: ['red'],
        labels: 'Evo evolution',
        parseTime: false,
        resize: true
    });
</script>
</body>

</html>
