<?php

/**
 * Created by PhpStorm.
 * User: Benjamin
 * Date: 06/05/2016
 * Time: 20:34
 */

class match extends rencontre
{
    function __construct()
    {
        parent::__construct();
    }


    /** vérifie qu'un joueur existe quand on crée un match
     * @param $pseudo
     * @return bool
     */
    function exisJoueur($pseudo){
        $sql="SELECT nb_elo FROM joueur WHERE UPPER(pseudo)=UPPER('$pseudo')";
        $res=$this->query($sql);
        if(pg_num_rows($res) == 1){
            if ($row = pg_fetch_row($res)) {
                return true;
            }
        }
        return false;
    }


    /** taille de rencontre_attente
     * @return bool
     */
    function lastId(){
        $sql="SELECT COUNT(id) FROM rencontre_attente";
        $res=$this->query($sql);
        if(pg_num_rows($res) == 1){
            if ($row = pg_fetch_row($res)) {
                return $row[0];
            }
        }
        return false;
    }

    /** taille de rencontre
     * @return bool
     */
    function Id(){
        $sql="SELECT COUNT(id) FROM rencontre";
        $res=$this->query($sql);
        if(pg_num_rows($res) == 1){
            if ($row = pg_fetch_row($res)) {
                return $row[0];
            }
        }
        return false;
    }

    function auxPseudo($pseudo){
        $db = new database();
        $sql = "SELECT email FROM joueur WHERE UPPER(pseudo) = UPPER('$pseudo')";
        $res = $this->query($sql);
        $row = pg_fetch_row($res);
        $sql2 = "SELECT pseudo FROM joueur WHERE email = '$row[0]'";
        $res2 = $this->query($sql2);
        return pg_fetch_row($res2)[0];
    }

    /** ajoute un match en match_attente
     * @param $p1
     * @param $pt1
     * @param $p2
     * @param $pt2
     * @param $gagnant
     * @return bool
     */
    function addMatch_attente($p1, $pt1, $p2, $pt2, $gagnant){
        $id = $this->lastId() + 1;
        $delta = ceil($this->calculDiffElo(abs($pt1-$pt2),$this->getDiffElo($p1,$p2)));
        $sql = "INSERT INTO rencontre_attente (id,pseudo1,pts1,pseudo2,pts2,gagnant,deltaelo) VALUES ('$id','$p1','$pt1','$p2','$pt2','$gagnant','$delta')";
        $res = $this->query($sql);
        if (!$res) {
            /*erreur*/
        }
        return true;
    }

    /** decrement tous les matchs après la suppression d'un match en attente
     * @param $id
     * @return bool
     */
    function decrement($id){
        $sql = "UPDATE rencontre_attente SET id = id - 1 WHERE id > $id";
        $res = $this->query($sql);
        if (!$res) {
            /*erreur*/
        }
        return true;
    }

    /** supprime un matche en attente
     * @param $id
     * @return bool
     */
    function delMatch_attente($id){
        $sql = "DELETE FROM rencontre_attente WHERE id = $id";
        $res = $this->query($sql);
        if (!$res) {
            /*erreur*/
        }
        return true;
    }

    /** ajoute un match en attente dans les tables rencontres et evo_elo
     * @param $id
     */
    function addMatch($id){
        $sql = "SELECT * FROM rencontre_attente WHERE id = $id";
        $res = $this->query($sql);
        $row = pg_fetch_row($res);
        $id2 = $this->Id() + 1;
        $sql2 = "INSERT INTO rencontre VALUES ('$id2','$row[1]','$row[2]','$row[3]','$row[4]','$row[5]','$row[6]','$row[7]')";
        $sql3 = "INSERT INTO evo_elo VALUES('$id2','$row[6]','$row[1]','$row[7]')";
        $sql4 = "INSERT INTO evo_elo VALUES('$id2','$row[4]','$row[1]','-$row[7]')";
        $db = new rencontre();
        $db->setNbelo($row[6], $db->getElo($row[6])+$row[7]);
        $db->setNbelo($row[4], $db->getElo($row[4])-$row[7]);
        $res2 = $this->query($sql2);
        $res3 = $this->query($sql3);
        $res4 = $this->query($sql4);
    }


    /**
     * mak les delta elo de la table rencontre attente
     */
    function updateElo(){
        $sql = "SELECT * FROM rencontre_attente";
        $res = $this->query($sql);
        while($row = pg_fetch_row($res)){
            $p1 = $row[6];
            $p2 = $row[4];
            $newelo = ceil($this->calculDiffElo(abs($row[3] - $row[5]),$this->getDiffElo($p1,$p2)));
            $sql2 = "UPDATE rencontre_attente SET deltaelo= '$newelo' WHERE id = $row[0]";
            $res2 = $this->query($sql2);
        }
    }


    
}