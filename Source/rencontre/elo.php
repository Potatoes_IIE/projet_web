<?php

/**
 * Created by PhpStorm.
 * User: Arthur
 * Date: 06/04/2016
 * Time: 14:52
 */


class rencontre extends database
{
    private $K= /**décommenter selon la valeurs/ le poids de la compétition**/
        15; /**classique**/ //il faudra surement créer une nouvelle classe pour chaque compétition
        //40; /**tournoi**/
        //60; /**ligue**/

    /**usage: connection à la BDD
     * match constructor.
     */
    function __construct()
    {
        parent::__construct();
    }

    /**
     * @return int
     */
    function getK()
    {
        return $this->K;
    }

    /* fonction qui nous permet d'avoir la différence de points (en valeur absolue) pour un match entre $pseudo1 et $pseudo2 */
    /* j'ai aussi ajouté une requete dans "lautre sens" pour que alex VS potatoes soit traité pareil que potatoes VS alex */
    function getDiffPoint($pseudo1, $pseudo2)
    {
        $sql1 = "SELECT pts1,pts2 FROM rencontre_attente WHERE pseudo1 ='$pseudo1' AND pseudo2='$pseudo2'";
        $sql2 = "SELECT pts1,pts2 FROM rencontre_attente WHERE pseudo1 ='$pseudo2' AND pseudo2='$pseudo1'";
        $res1 = $this->query($sql1);
        $res2 = $this->query($sql2);

        if ($row = pg_fetch_row($res1)) {
            if (pg_num_rows($res1) == 2) {
                return abs($row[0] - $row[1]);
            }
        }
        if ($row = pg_fetch_row($res2)) {
            if (pg_num_rows($res2) == 2) {
                return abs($row[0] - $row[1]);
            }
        }

        return false;
    }

    function setNbelo($pseudo, $newelo)
    {
        $sql = "UPDATE joueur SET nb_elo='$newelo' WHERE pseudo ='$pseudo'";
        $res = $this->query($sql);
        if ($row = pg_fetch_row($res)) {
            if (pg_num_rows($res) == 1) {
                return true;
            }
        }
        return false;
    }


    /* fonction pour obtenir les points elo de 2 joueurs */
    function getElo2($pseudo1, $pseudo2)
    {
        $j1 = $this->getElo($pseudo1);
        $j2 = $this->getElo($pseudo2);
        return [$j1, $j2];
    }

    function getDiffElo($pseudo1,$pseudo2){
        $elo = $this->getElo2($pseudo1,$pseudo2);
        return $elo[0]-$elo[1];
    }

    /** calcul le delta Elo d'un match
     * @param $delta
     * @param $diffElo
     * @return int
     */
    function calculDiffElo($delta,$diffElo){
        $K=$this->getK();
        if($delta<2){
            return $K*(1-(1/(1+10**(-$diffElo/400))));
        }
        elseif($delta==2){
            return $K*(1+(1/2))*(1-(1/(1+10**(-$diffElo/400))));
        }
        elseif($delta==3){
            return $K*(1+(3/4))*(1-(1/(1+10**(-$diffElo/400))));
        }
        else {
            return $K*(1+(3/4)+(($delta-3)/8))*(1-(1/(1+10**(-$diffElo/400))));
        }
    }

    /** récupère la proba de gagner de pseudo 1 contre pseudo 2
     * @param $pseudo1
     * @param $pseudo2
     * @return int
     */
    function getGain($pseudo1, $pseudo2){
        $K=$this->getK();
        $diffLElo = $this->getDiffElo($pseudo1,$pseudo2);
        return 1-($this->calculDiffElo(1,$diffLElo)/$K);
    }

    /**fonction qui a l'issue d'une rencontre, modifie les pts elo des joueurs en conséquence de l'issue du match
     * pseudo1=gagnant pseudo2=perdant
     * @param $pseudo1
     * @param $pseudo2
     * @requires : pseudo1 est le gagnant
     * @return bool
     */
    function setMatch($pseudo1, $pseudo2)
    {
        /* on recupere les nombre elo de chacun des joueurs */
        $elo1 = $this->getElo($pseudo1);
        $elo2 = $this->getElo($pseudo2); /* on calcule la difference de buts dans le match */
        $diffpts = $this->getDiffPoints($pseudo1, $pseudo2);
        /* on calcule le nb de pts elo a ajouter/enlever au gagnant/perdant */
        $diffelo = ceil($this->calculDiffElo($diffpts, $this->getDiffElo($pseudo1, $pseudo2)));
        /* nouvelle affectation des nb_elo */
        $elo1 = $elo1 + ($diffelo);
        $elo2 = $elo2 - ($diffelo);
        if ($elo1 < 0) {
            $elo = 0;
        } //on limite le score minimal à 0
        elseif ($elo2 < 0) {
            $elo2 = 0;
        }
        /* MAJ dans la BDD */
        $sql = "UPDATE joueur set nb_elo='$elo1' WHERE pseudo ='$pseudo1'";
        $sql2 = "UPDATE joueur set nb_elo='$elo2' WHERE pseudo ='$pseudo2'";
        $res = $this->query($sql);
        $res2 = $this->query($sql2);/* verification bonne execution */
        if ($row = pg_fetch_row($res) && $row2 = pg_fetch_row($res2)) {
            if (pg_num_rows($res) == 1 && pg_num_rows($res2) == 1) {
                return true;
            }
        }
        return false;
    }
    
    /* ici commence l'implémentation pour traiter les match par equipes */

    /* change le nb de pts elo d'un jour dans toute les équipe ou il est */
    function setPts_elo_j1($pseudo, $newelo)
    {
        $sql = "UPDATE equipe SET pts_elo_j1='$newelo' WHERE joueur1 ='$pseudo' ";
        $sql2 = "UPDATE equipe SET pts_elo_j='$newelo' WHERE joueur2 ='$pseudo' ";
        $res = $this->query($sql);
        $res2 = $this->query($sql2);
        if ($row = pg_fetch_row($res)) {
            if (pg_num_rows($res) == 2) {
                return true;
            }
        }
    }

    function setPts_elo_j2($pseudo, $newelo)
    {
        $sql = "UPDATE equipe SET pts_elo_j2='$newelo' WHERE pseudo ='$pseudo'";
        $res = $this->query($sql);
        if ($row = pg_fetch_row($res)) {
            if (pg_num_rows($res) == 1) {
                return true;
            }
        }
    }

    /* permet d'obtenir l'ID de l'quipe en fonction des joueurs qui la compose */
    function getIdEquipe($pseudo1, $pseudo2)
    {
        $sql1 = "SELECT id FROM equipe WHERE joueur1='$pseudo1' AND joueur2='$pseudo2'";
        $sql2 = "SELECT id FROM equipe WHERE joueur1='$pseudo2' AND joueur2='$pseudo1'";
        $res1 = $this->query($sql1);
        $res2 = $this->query($sql2);

        if ($row = pg_fetch_row($res1)) {
            if (pg_num_rows($res1) == 1) {
                return ($row[0]);
            }
        }
        if ($row = pg_fetch_row($res2)) {
            if (pg_num_rows($res2) == 1) {
                return ($row[0]);
            }
        }
    }

    /* fonction qui renvoie le nombre elo total d'une equipe ( alex & jordan ou jordan & alex), via l'id unique */
    function getEloEquipe($id)
    {
        $sql1 = "SELECT pts_elo_j1,pts_elo_j2 FROM equipe WHERE id='$id'";
        $res1 = $this->query($sql1);

        if ($row = pg_fetch_row($res1)) {
            if (pg_num_rows($res1) == 2) {
                return (0.5 * ($row[0] + $row[1]));
            }
        }
        return false;
    }

    /* fonction similaire mais qui utilise une jointure ! */
    function getEloEquipe2($pseudo1,$pseudo2)
    {
        $sql1 = "SELECT nb_elo FROM equipe JOIN joueur ON equipe.joueur1 = joueur.pseudo WHERE pseudo='$pseudo1'";
        $sql2 = "SELECT nb_elo FROM equipe JOIN joueur ON equipe.joueur2 = joueur.pseudo WHERE pseudo='$pseudo2'";
        $res1 = $this->query($sql1);
        $res2 = $this->query($sql2);

        if ($row = pg_fetch_row($res1) && $row2= pg_fetch_row($res2)) {
            if (pg_num_rows($res1) == 1 && pg_num_rows($res2)) {
                return (0.5 * ($row[0] + $row2[0]));
            }
        }
        return false;
    }
    /* fonction qui renvoie la difference de points dans un match : equipe 1 VS equipe 2 ou equipe 2 VS equipe 1 */

    function getDiffPointEquipe($id1, $id2)
    {
        $sql1 = "SELECT pts_equipe_1,pts_equipe_2 FROM rencontre_equipe WHERE id_equipe1 ='$id1' AND pseudo2='$id2'";
        $sql2 = "SELECT pts_equipe_1,pts_equipe_2 FROM rencontre_equipe WHERE id_equipe2 ='$id2' AND pseudo2='$id1'";
        $res1 = $this->query($sql1);
        $res2 = $this->query($sql2);

        if ($row = pg_fetch_row($res1)) {
            if (pg_num_rows($res1) == 2) {
                return abs($row[0] - $row[1]);
            }
        }
        if ($row = pg_fetch_row($res2)) {
            if (pg_num_rows($res2) == 2) {
                return abs($row[0] - $row[1]);
            }
        }

        return false;
    }
    /* fonction qui a l'issue d'une rencontre, modifie les pts elo des joueurs en conséquence de l'issue du match par équipe */
    /* pseudo1 et pseudo2 = gagnant pseudo 3 et pseudo4 =  perdant */
    function setMatchEquipe($pseudo1, $pseudo2, $pseudo3, $pseudo4)
    {
        /* on recupere les ID des 2 équipes */
        $id_equipe1 = $this->getIdEquipe($pseudo1, $pseudo2);
        $id_equipe2 = $this->getIdEquipe($pseudo3, $pseudo4);
        /* on recupere le nb elo associé a chaque equipe */
        $eloequipe1 = $this->getEloEquipe($id_equipe1);
        $eloequipe2 = $this->getEloEquipe($id_equipe2);
        /* on recupere la différence de pts du match */
        $diffpts = $this->getDiffPointsEquipe($id_equipe1, $id_equipe2);
        /* on calcul la difference de pts elo entr les 2 equipes */
        $diff_pts_elo = abs($eloequipe2 - $eloequipe1);
        /* on calcul le nb de pots elo a ajouter/enlever au gagnant/perdant */
        $diffelo = $this->calculDiffElo($diffpts, $diff_pts_elo);
        /* on fait les nouvelles affecation des points elo */
        $eloequipe1 = $eloequipe1 + ($diffelo);
        $eloequipe2 = $eloequipe2 - ($diffelo);
        if ($eloequipe1 < 0) {
            $eloequipe1 = 0;
        } //on limite le score minimal à 0
        elseif ($eloequipe2 < 0) {
            $eloequipe2 = 0;
        }
        /* on recupere les nb elo de tout les joueurs pour midifier les points elo des joueurs des  equipes */
        $elo1 = $this->getElo($pseudo1);
        $elo2 = $this->getElo($pseudo2);
        $elo3 = $this->getElo($pseudo3);
        $elo4 = $this->getElo($pseudo4);
        /* on les modifie en les pondérant */
        $elo1 = $elo1 + $diffelo * ($elo2 / $eloequipe1); /* + pour les gagants */
        $elo2 = $elo2 + $diffelo * ($elo1 / $eloequipe1);
        $elo3 = $elo3 - $diffelo * ($elo3 / $eloequipe2); /* - pour les perdants */
        $elo4 = $elo4 - $diffelo * ($elo4 / $eloequipe2);
        /* MAJ dans la BDD  table joueur et table equipe*/
        $sql0="UPDATE equipe SET pts_elo_j1=$elo1 WHERE id='$id_equipe1'";
        $sql00="UPDATE equipe SET pts_elo_j2=$elo3 WHERE id='$id_equipe1'";
        $sql000="UPDATE equipe SET pts_elo_j1= $elo3 WHERE id='$id_equipe2'";
        $sql0000="UPDATE equipe SET pts_elo_j2=$elo4 WHERE id='$id_equipe2'";
        $sql11 = "UPDATE joueur set nb_elo='$elo1' WHERE pseudo ='$pseudo1'";
        $sql12 = "UPDATE joueur set nb_elo='$elo2' WHERE pseudo ='$pseudo2'";
        $sql21 = "UPDATE joueur set nb_elo='$elo3' WHERE pseudo ='$pseudo3'";
        $sql22 = "UPDATE joueur set nb_elo='$elo4' WHERE pseudo ='$pseudo4'";
        $res0 = $this->query($sql0);
        $res00 = $this->query($sql00);
        $res000 = $this->query($sql000);
        $res0000 = $this->query($sql0000);
        $res11 = $this->query($sql11);
        $res12 = $this->query($sql12);
        $res21 = $this->query($sql21);
        $res22 = $this->query($sql22);
        /* verif bonne execution des 8 requetes d'update */
        if ( $row0 = pg_fetch_row($res0) && $row00 = pg_fetch_row($res00) && $row000 = pg_fetch_row($res000) &&
                    $row0000 = pg_fetch_row($res0000) && $row = pg_fetch_row($res11)
                            && $row2 = pg_fetch_row($res12)
                                && $row3 = pg_fetch_row($res21) && $row4 = pg_fetch_row($res22)) {
            if (pg_num_rows($res11) == 1 && pg_num_rows($res12) == 1 && pg_num_rows($res21) == 1 && pg_num_rows($res22) == 1 &&
                pg_num_rows($res0) == 1 && pg_num_rows($res00) == 1 && pg_num_rows($res000) == 1 && pg_num_rows($res0000) == 1) {
                return true;
            }
        }
        return false;

    }


    /* fonction qui fait pareil que exportStatEquipe mais qui traite les match par equipe */
    function exportStatEquipe($pseudo1, $pseudo2)
    {
        $id = $this->$this->getIdEquipe($pseudo1,$pseudo2);
        $sql="SELECT pts_elo_j1, pts_elo_j2 FROM equipe WHERE pseudo='$id'";
        $res=$this->query($sql);
        if($row= pg_fetch_row($res)){
            if(pg_num_rows($res) == 1){
                $sql2="INSERT INTO evo_elo(id,joueur,date,pts_elo) VALUES (,$pseudo1,CURRENT_DATE,$row[0])";
                $sql22="INSERT INTO evo_elo(id,joueur,date,pts_elo) VALUES (,$pseudo2,CURRENT_DATE,$row[1])";
                $res2=$this->query($sql22);
                if ($row2= pg_fetch_row($res2)) {
                    if (pg_num_rows($res2) == 1) {
                        return true;
                    }
                }
            }
        }
        return false;

    }
}














