/**CLASSE DE FONCTION POUR LA PAGE D'AUTHENTIFICATION !!*/

$(document).ready(function() {
	'use strict';
	$(".tab-gr li a").on('click', function (e) { //permet le changement entre les deux fenêtre
		e.preventDefault();
		$(".tab-gr li a").removeClass("active");
		$(this).addClass("active");
		var href = $(this).attr('href');
		$('.show')
			.removeClass('show')
			.addClass('hide')
			.hide();
		$(href)
			.removeClass('hide')
			.addClass('show')
			.hide()
			.fadeIn(550);
	});
	$('#cpass').on('keyup', function () { //pour "s'enregistrer": permet de vérifier si c'est le même mot de passe
		if ($(this).val() == $('#pass').val()) {
			$(this)
				.removeClass('bad')
				.addClass('good');
            $('input[type="submit"]').prop('disabled', false);
		}
		else {
			$(this)
				.removeClass('good')
				.addClass('bad');
            $('input[type="submit"]').prop('disabled', true);
		}
	});
    $('#email').on('keyup', function (){ //pour "se connecter": permet de vérifier qu'on a bien un email et desactive ou pas le boutton
		var reg = new RegExp('^[a-z0-9]+([_|\.|-]{1}[a-z0-9]+)*@[a-z0-9]+([_|\.|-]{1}[a-z0-9]+)*[\.]{1}[a-z]{2,6}$', 'i');
        if(reg.test($(this).val())){
            $(this)
				.removeClass('bad')
				.addClass('good');
            $('input[type="submit"]').prop('disabled', false);
        }
        else {
            $(this)
				.removeClass('good')
				.addClass('bad');
            $('input[type="submit"]').prop('disabled', true);
        }
    });
    $('#email2').on('keyup', function (){ //pour "s'enregistrer": permet de vérifier qu'on a bien un email et desactive ou pas le boutton
        var reg = new RegExp('^[a-z0-9]+([_|\.|-]{1}[a-z0-9]+)*@[a-z0-9]+([_|\.|-]{1}[a-z0-9]+)*[\.]{1}[a-z]{2,6}$', 'i');
        if(reg.test($(this).val())){
            $(this)
                .removeClass('bad')
                .addClass('good');
            $('input[type="submit"]').prop('disabled', false);
        }
        else {
            $(this)
                .removeClass('good')
                .addClass('bad');
            $('input[type="submit"]').prop('disabled', true);
        }
    });
	
});