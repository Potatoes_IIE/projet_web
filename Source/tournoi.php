<?php
if(isset($_GET['ins'])){$ins = $_GET['ins'];};

session_start();
/**
 * renvoie le Pseudo de l'user courant pour la bar de nav
 */
function user(){
    echo $_SESSION['user_pseudo']; //recupere le pseudo de la session
}
include 'DB/dbhelp.php';
include 'DB/tour.php';
include 'html/test_login.php';
include "html/info_user.php";
include 'config.php';
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>BabIIE</title>
    <!-- Bootstrap Core CSS -->
    <link href="styles/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="styles/style-site.css" rel="stylesheet">
    <!-- Morris Charts CSS -->
    <link href="styles/morris.css" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
    <!-- Custom Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link rel="shortcut icon" type="image/png" href="images/ico2.png"/>
</head>

<body>
<div id="wrapper">
    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top top-nav" role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <a href="accueil.php" class="logo text-center"><img src="images/nav.png" alt="Logo de ouf"></a>
            <a class="navbar-brand" href="accueil.php">BabIIE</a>
        </div>
        <!-- Top Menu Items -->
        <ul class="nav navbar-right top-nav">
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="glyphicon glyphicon-user"></i> <?php user();?><span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li>
                        <a href="profil.php"><i class="glyphicon glyphicon-user" style="color: #595a58;"></i> Profile</a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a href="html/logout.php"><i class="glyphicon glyphicon-log-out" style="color: #595a58;"></i> Log Out</a>
                    </li>
                </ul>
            </li>
        </ul>
        <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
        <div class="collapse navbar-collapse navbar-ex1-collapse">
            <ul class="nav navbar-nav side-nav">
                <li>
                    <div class="block padd"><a href="new_match.php"><button type="button" class="btn btn-primary center-block">ADD NEW MATCH</button></a></div>
                </li>
                <?php
                if($_SESSION['user_admin']){
                    echo "<li>
                    <div class=\"block padd\"><a href=\"match_attente.php\"><button type=\"button\" class=\"btn btn-danger center-block\">SEE ALL MATCH</button></a></div>
                </li>";
                }
                ?>
                <li>
                    <a href="accueil.php"><i class="fa fa-fw fa-home"></i>  ACCUEIL </a>
                </li>
                <li>
                    <a href="classement.php"><i class="fa fa-fw fa-list-alt"></i> CLASSEMENT</a>
                </li>
                <li>
                    <a href="tournoi.php" class="active"><i class="fa fa-fw fa-sitemap"></i> TOURNOI</a>
                </li>
                <li>
                    <a href="ligue.php"><i class="fa fa-fw fa-bolt"></i> LIGUE</a>
                </li>
                <li>
                    <a href="reglement.php"><i class="fa fa-fw fa-cogs"></i> REGLEMENT</a>
                </li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </nav>

    <div id="page-wrapper">

        <div class="container-fluid">

            <!-- Page Heading -->
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">
                        BabIIE <small>Tournoi</small>
                    </h1>
                </div>
            </div>
            
            <?php /*POUR AFFICHER OU NON LE BOUTON NEW TOURNOI SI ON EST ADMIN OU PAS !*/
            if($_SESSION['user_admin'] && ! $_SESSION['tournoi']) {
                echo "<div class=\"row\">
                <div class=\"col-lg-offset-11\">
                    <a href=\"php/new_tournoi.php\"><button type=\"button\" class=\"btn btn-primary center-block\">NEW TOURNOI !</button></a>
                </div>
            </div>
            <br>";
            }
            ?>

            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h1 class="panel-title"><i class="fa fa-fw fa-info-circle"></i> A VENIR</h1>
                        </div>
                        <div class="panel-body">
                            Prochainement, de nouvelles fonctionnalités pour la réalisation de tournois et pour y participer !
                            <?php echo $_SESSION['tournoi'];
                            echo $_SESSION['tournoi_taille'];
                            ?>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <?php
                $tournoi = new tour();
                $taille_t = $_SESSION['tournoi_taille'];
                $taille = $tournoi->taille();
                $place = $taille_t - $taille;
                if($_SESSION['tournoi'] && $tournoi->testJoueur($_SESSION['user_pseudo']) && ($tournoi->taille() < $_SESSION['tournoi_taille'])) {
                    echo "<a href=\"php/inscription.php\"><button type=\"button\" class=\"btn btn-primary center-block huge\">INSCRIVEZ-VOUS ! <h2>$place place(s)</h2></button></a>";
                    echo $taille_t - $taille;
                }
                elseif (isset($ins)) {
                    if($ins==1 && $_SESSION['tournoi']){
                        echo "<h3 class=\"text-center\">Merci de votre inscription !</h3>";
                    }
                    else {
                        echo "<h3 class=\"text-center\">Tournois à venir !</h3>";
                    }
                }
                elseif(($tournoi->taille() >= $_SESSION['tournoi_taille']) && $_SESSION['tournoi']){
                    echo "<h3 class=\"text-center\">Tournoi plein, référez-vous aux admins pour la suite !</h3>";
                }
                elseif($_SESSION['tournoi'] && !$tournoi->testJoueur($_SESSION['user_pseudo'])){
                    echo "<h3 class=\"text-center\">Vous êtes déjà inscrit !</h3>";
                }
                elseif (!$_SESSION['tournoi']) {
                    echo "<h3 class=\"text-center\">Tournois à venir !</h3>";
                }

                ?>
            </div>

            <div class="row col-lg-4 col-lg-offset-4">
                <div class="table-responsive">
                    <table class="table table-hover table-striped">
                        <thead>
                        <?php
                        include("DB/form.php");
                        include("php/classement_index.php");
                        top_t();
                        ?>
            </div>

            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /#page-wrapper -->
</div>



<!-- /#wrapper -->
<script src="script/jquery.min.js"></script>

<script>
    // ADD SLIDEDOWN ANIMATION TO DROPDOWN //
    $('.dropdown').on('show.bs.dropdown', function(e){
        $(this).find('.dropdown-menu').first().stop(true, true).slideDown();
    });

    // ADD SLIDEUP ANIMATION TO DROPDOWN //
    $('.dropdown').on('hide.bs.dropdown', function(e){
        $(this).find('.dropdown-menu').first().stop(true, true).slideUp();
    });
</script>
<script src="styles/js/bootstrap.min.js"></script>
<script src="styles/js/morris/raphael.min.js"></script>
<script src="styles/js/morris/morris.min.js"></script>
<script src="styles/js/morris/morris-data.js"></script>
<script>

</script>
</body>

</html>
