<?php
/**
 * Created by PhpStorm.
 * User: Benjamin
 * Date: 07/04/2016
 * Time: 16:25
 * vérification des cas d'erreurs de la création d'un compte
 */

//begin session
session_start();
include ("../DB/dbhelp.php");
include("../DB/form.php");

// on récupère les entrés puis on va faire les vérifications !
$mail = $_POST['email'];
$mdp = $_POST['pass'];
$cmdp = $_POST['cpass'];
$nom = $_POST['nom'];
$prenom = $_POST['prenom'];
$pseudo = $_POST['pseudo'];
$token = $_POST['form_token'];

/*** first check that both the username, password, c_password and form token have been sent ***/
if(!isset($mail,$mdp,$cmdp,$token,$nom,$prenom,$pseudo))
{
    header("Location: ../index.php?error=2");
}
/*** check the form token is valid ***/
elseif($token != $_SESSION['form_token'])
{
    header("Location: ../index.php?error=2");
}
/*** check the password has only alpha numeric characters ***/
elseif (ctype_alnum($mdp) != true && ctype_alnum($cmdp) != true)
{
    /*** if there is no match ***/
    header("Location: ../index.php?error=2");
}

else
{
    /*** if we are here the data is valid and we can insert it into database ***/
    $mail = filter_var($mail, FILTER_VALIDATE_EMAIL);
    $mdp = filter_var($mdp, FILTER_SANITIZE_STRING);
    $nom = filter_var($nom, FILTER_SANITIZE_STRING);
    $prenom = filter_var($prenom, FILTER_SANITIZE_STRING);
    $pseudo = filter_var($pseudo, FILTER_SANITIZE_STRING);

    /*** connect to database ***/
    $db = new form();
    if ($db->testMail($mail) && $db->testPseudo($pseudo)){
        if($db->setClient($nom,$pseudo,$prenom,$mail,$mdp)){
            $_SESSION['user_pseudo']= $pseudo;
            header("Location: ../accueil.php?type=1");
        }
    }
    else {
        header("Location: ../index.php?error=2");  /**permet d'afficher l'erreur defini dans index.php**/
    }
}
