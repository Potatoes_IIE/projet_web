<?php
session_start();
/**
 * renvoie le Pseudo de l'user courant pour la bar de nav
 */
function user(){
    echo $_SESSION['user_pseudo']; //recupere le pseudo de la session
}
include '../DB/dbhelp.php';
include '../html/test_login.php';
include '../html/test_admin.php';
include "../html/info_user.php";
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>BabIIE</title>
    <!-- Bootstrap Core CSS -->
    <link href="../styles/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="../styles/style-site.css" rel="stylesheet">
    <!-- Morris Charts CSS -->
    <link href="../styles/morris.css" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
    <!-- Custom Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link rel="shortcut icon" type="image/png" href="../images/ico2.png"/>
</head>

<body>
<div id="wrapper">
    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top top-nav" role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <a href="../accueil.php" class="logo text-center"><img src="../images/nav.png" alt="Logo de ouf"></a>
            <a class="navbar-brand" href="../accueil.php">BabIIE</a>
        </div>
        <!-- Top Menu Items -->
        <ul class="nav navbar-right top-nav">
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="glyphicon glyphicon-user"></i> <?php user();?><span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li>
                        <a href="#"><i class="glyphicon glyphicon-user" style="color: #595a58;"></i> Profile</a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a href="../html/logout.php"><i class="glyphicon glyphicon-log-out" style="color: #595a58;"></i> Log Out</a>
                    </li>
                </ul>
            </li>
        </ul>
        <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
        <div class="collapse navbar-collapse navbar-ex1-collapse">
            <ul class="nav navbar-nav side-nav">
                <li>
                    <div class="block padd"><button type="button" class="btn btn-primary center-block">ADD NEW MATCH</button></div>
                </li>
                <li>
                    <a href="../accueil.php" class="active"><i class="fa fa-fw fa-home"></i>  ACCUEIL </a>
                </li>
                <li>
                    <a href="../classement.php"><i class="fa fa-fw fa-list-alt"></i> CLASSEMENT</a>
                </li>
                <li>
                    <a href="../tournoi.php"><i class="fa fa-fw fa-sitemap"></i> TOURNOI</a>
                </li>
                <li>
                    <a href="../ligue.php"><i class="fa fa-fw fa-bolt"></i> LIGUE</a>
                </li>
                <li>
                    <a href="../reglement.php"><i class="fa fa-fw fa-cogs"></i> REGLEMENT</a>
                </li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </nav>

    <div id="page-wrapper">

        <div class="container-fluid">

            <!-- Page Heading -->
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">
                        BabIIE <small>New Tournoi</small>
                    </h1>
                </div>
            </div>
            
            <form action="add_tournoi.php" method="post">
                <div class="col-lg-1 text-center">
                    <input type="number" class="form-control bfh-number" value="5" name="taille">Taille du tournoi en puissance de 2
                    <input type="submit" name="next" class="btn btn-primary center-block" value="C'est parti !" />
                </div>
            </form>


            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /#page-wrapper -->
</div>



<!-- /#wrapper -->
<script src="../script/jquery.min.js"></script>

<script>
    // ADD SLIDEDOWN ANIMATION TO DROPDOWN //
    $('.dropdown').on('show.bs.dropdown', function(e){
        $(this).find('.dropdown-menu').first().stop(true, true).slideDown();
    });

    // ADD SLIDEUP ANIMATION TO DROPDOWN //
    $('.dropdown').on('hide.bs.dropdown', function(e){
        $(this).find('.dropdown-menu').first().stop(true, true).slideUp();
    });
</script>
<script src="../styles/js/bootstrap.min.js"></script>
<script src="../styles/js/morris/raphael.min.js"></script>
<script src="../styles/js/morris/morris.min.js"></script>
<script src="../styles/js/morris/morris-data.js"></script>
<script>

</script>
</body>

</html>

