<?php
/**
 * Created by PhpStorm.
 * User: Benjamin
 * Date: 07/04/2016
 * Time: 16:25
 */

//begin session
session_start();
include ("../DB/dbhelp.php");
include("../DB/form.php");

// on récupère les entrés puis on va faire les vérifications !
$pseudo = $_SESSION['user_pseudo'];
$mail = $_POST['email'];
$mdp = $_POST['pass'];
$cmdp = $_POST['cpass'];
$nom = $_POST['nom'];
$prenom = $_POST['prenom'];
$db = new form();

/*** first check that both the username, password, c_password and form token have been sent ***/
if(!isset($_POST['pass'],$_POST['cpass'])){
    if($db->testMail($mail) || ($mail == $_SESSION['user_email'])) {
        if ($db->setPrenom($prenom, $pseudo) && $db->setNom($nom, $pseudo) && $db->setMail($mail, $pseudo)) {
            header("Location: ../profil.php");
        }
    }
    else {
        header("Location: ../profil.php?type=2");
    }
}
elseif(isset($mdp,$cmdp)){
    if($mdp != $cmdp){
        header("Location: ../profil.php?type=4");
    }
    elseif($db->testMail($mail) || ($mail == $_SESSION['user_email'])) {
        if ($db->setPrenom($prenom, $pseudo) && $db->setNom($nom, $pseudo) && $db->setMail($mail, $pseudo) && $db->setPass($mdp,$pseudo)) {
            header("Location: ../profil.php");
        }
    }
    else {
        header("Location: ../profil.php?type=2");
    }
}
else {
    header("Location: ../profil.php?type=3");
}