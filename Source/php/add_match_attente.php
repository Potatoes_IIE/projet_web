<?php
/**
 * Created by PhpStorm.
 * User: Benjamin
 * Date: 06/05/2016
 * Time: 20:32
 * ajoute un match dans rencontre_attente
 */
session_start();
include '../DB/dbhelp.php';
include '../rencontre/elo.php';
include '../rencontre/match.php';

$match = new match();
$p1 = $_SESSION['user_pseudo'];
$p2 = $_POST['p2'];
$pt1 = $_POST['pt1'];
$pt2 = $_POST['pt2'];

if(!$match->exisJoueur($p2)){
    header("Location: ../new_match.php?error=1");
}
elseif($pt1 == $pt2){
    header("Location: ../new_match.php?error=2");
}
else {
    $p2 = $match->auxPseudo($p2);
    if($pt1 > $pt2){
        $gagnant = $p1;
        $match->addMatch_attente($p1,$pt1,$p2,$pt2,$gagnant);
    }
    else {
        $gagnant = $p2;
        $match->addMatch_attente($p2,$pt2,$p1,$pt1,$gagnant);
    }
    header("Location: ../accueil.php");
}