<?php

/**
 * génère le classement des 5 premiers
 */
function consult(){
	$DB = new database();
    print '<table class="classement">';
	print '<tr><th>Classement</th><th>Pseudo</th></tr>';
	$classement = $DB->Classement(5,0);
    if(!$classement){
        /*erreur*/
    }
    else {
        $x = 1;
        foreach ($classement as $pseudo => $elo) {
            echo "<tr><td>$x</td><td>$pseudo <div style='display:block; font-size:15px; color:#df2020'>$elo pts</div></td></tr>";
            $x++;
        }
    }
	echo '</table>';
}

/**
 * fonction qui créer le classement de la page d'accueil dans le style voulu
 */
function consult2(){
    $DB = new database();
    print '<tr><th>Classement</th><th>Pseudo</th></tr>';
    $classement = $DB->Classement(5,0);
    if(!$classement){
        /*erreur*/
    }
    else {
        $x = 1;
        foreach ($classement as $pseudo => $elo) {
            echo "<tr><td>$x</td><td>$pseudo <div style='display:block; font-size:13px; color:#df2020'>$elo pts</div></td></tr>";
            $x++;
        }
    }
    echo '</table>';
}

/** nous permet de calculer l'ensemble des buts marqués et encaissé d'un joueur
 * @param $pseudo
 * @return array
 */
function bmBE($pseudo){
    $db = new database();
    $sql = "SELECT pts1,pts2,gagnant  FROM rencontre WHERE pseudo1 = '$pseudo' OR pseudo2 ='$pseudo'";
    $res = $db->query($sql);
    $bm = 0;
    $be = 0;
    while($row = pg_fetch_row($res)){
        if($row[2] == $pseudo){
            $bm += $row[0];
            if($row[1] < 0){
                $bm += abs($row[1]);
            }
            else {
                $be += $row[1];
            }
        }
        else{
            $be += $row[0];
            if($row[1] < 0){
                $be += abs($row[1]);
            }
            else {
                $bm += $row[1];
            }
        }
    }
    return [$bm, $be];
}

/** fonction qui créer le tableau du classement de tous les joueurs
 * @param $taille
 * @param $offset
 * @param $cla
 */
function top($taille, $offset, $cla){
    $db = new database();
    print '<tr><th>#</th><th>Pseudo</th><th>Points Elo</th><th>Total Match</th><th>Total Victoire</th><th>Total Defaite</th><th>Ratio Vict/Def</th><th>Buts Marqués</th><th>Buts Encaissés</th><th>Ratio BM/BE</th></tr>';
    print '</thead>';
    print '<tbody>';
    $classement = $db->Classement($taille,$offset);
    if(!$classement){
        /*erreur*/
    }
    else {
        $x = 1;
        foreach ($classement as $pseudo => $elo) {
            $sql = "SELECT COUNT(id) FROM rencontre WHERE pseudo1 = '$pseudo' OR pseudo2 = '$pseudo'";
            $sql2 = "SELECT COUNT(id) FROM rencontre WHERE gagnant = '$pseudo'";
            $res = $db->query($sql);
            $res2 = $db->query($sql2);
            $row = pg_fetch_row($res);
            $row2 = pg_fetch_row($res2);
            $defaite = $row[0] - $row2[0];
            $b = bmBE($pseudo);
            if($b[1] == 0){
                $ratio2 = $b[0];
            }
            else {
                $ratio2 = round($b[0]/$b[1],2);
            }
            if($defaite == 0){
                $ratio = $row2[0];
            }
            else {
                $ratio = round($row2[0]/$defaite, 2);
            }
            if($x == $cla){
                echo "<tr class=\"danger\"><td>$x</td><td>$pseudo</td><td><strong>$elo</strong></td><td>$row[0]</td><td>$row2[0]</td><td>$defaite</td><td>$ratio</td><td>$b[0]</td><td>$b[1]</td><td>$ratio2</td></tr>";
                $x++;
            }
            else {
                echo "<tr><td>$x</td><td>$pseudo</td><td><strong>$elo</strong></td><td>$row[0]</td><td>$row2[0]</td><td>$defaite</td><td>$ratio</td><td>$b[0]</td><td>$b[1]</td><td>$ratio2</td></tr>";
                $x++;
            }
        }
    }
    print '</tbody>';
    print '</table>';
}

/**
 * fonction qui créer le tableau des joueurs inscrits pour le tournoi en cours
 */
function top_t(){
    $DB = new tour();
    print '<tr><th class="tournois">#</th><th class="tournois">Pseudo</th><th class="tournois">Points Elo</th></tr>';
    print '</thead>';
    print '<tbody>';
    $classement = $DB->listeInscrit_tournois();
    if(!$classement){
        /*erreur*/
    }
    else {
        $x = 1;
        foreach ($classement as $pseudo => $elo) {
            echo "<tr><td>$x</td><td>$pseudo</td><td>$elo pts</td></tr>";
            $x++;
            
        }
    }
    print '</tbody>';
    print '</table>';
}

/**
 * génère la table des matchs en attentes avec les différentes actions
 */
function consult_attente(){
    $db = new database();
    print '<tr><th>Id</th><th>Date</th><th>Pseudo 1</th><th>Point 1</th><th>Pseudo 2</th><th>Point 2</th><th>Gagnant</th><th>Delta Elo</th><th>Action</th></tr>';
    print '</thead>';
    print '<tbody>';
    $sql = "SELECT * FROM rencontre_attente ORDER BY id ASC";
    $res = $db->query($sql);
    while(($row = pg_fetch_row($res))){
        echo "<tr><td>$row[0]</td><td>$row[1]</td><td>$row[2]</td><td>$row[3]</td><td>$row[4]</td><td>$row[5]</td><td>$row[6]</td><td>$row[7]</td><td><a href=\"php/add_match.php?id=$row[0]\"><button type=\"button\" class=\"btn btn-success btn-xs btn-round\"><span class=\"glyphicon glyphicon-ok\"></span></button></a> <a href=\"php/del_match_attente.php?id=$row[0]\"><button type=\"button\" class=\"btn btn-danger btn-xs btn-round\"><span class=\"glyphicon glyphicon-remove\"></span></button></a></td></tr>";
    }
    echo '</tbody>';
    echo '</table>';
}

/** table des statistiques du joueur
 * @param $pseudo
 */
function stat_profil($pseudo){
    $db = new database();
    $sql = "SELECT COUNT(id) FROM rencontre WHERE pseudo1 = '$pseudo' OR pseudo2 = '$pseudo'";
    $sql2 = "SELECT COUNT(id) FROM rencontre WHERE gagnant = '$pseudo'";
    $sql3 = "SELECT nb_elo, date_ins FROM joueur WHERE pseudo = '$pseudo'";
    $res = $db->query($sql);
    $res2 = $db->query($sql2);
    $res3 = $db->query($sql3);
    print '<tr><th>Date d\'inscription</th><th>Total Match</th><th>Victoire</th><th>Defaite</th><th>Ratio V/D</th><th>BM</th><th>BE</th><th>Ratio BM/BE</th><th>Point Elo</th></tr>';
    print '</thead>';
    print '<tbody>';
    $row = pg_fetch_row($res);
    $row2 = pg_fetch_row($res2);
    $row3 = pg_fetch_row($res3);
    $defaite = $row[0] - $row2[0];
    $b = bmBE($pseudo);
    if($b[1] == 0){
        $ratio2 = $b[0];
    }
    else {
        $ratio2 = round($b[0]/$b[1],2);
    }
    if($defaite == 0){
        $ratio = $row2[0];
    }
    else {
        $ratio = round($row2[0]/$defaite, 2);
    }
    echo "<tr><td>$row3[1]</td><td>$row[0]</td><td>$row2[0]</td><td>$defaite</td><td>$ratio</td><td>$b[0]</td><td>$b[1]</td><td>$ratio2</td><td>$row3[0] pts</td></tr>";
    echo '</tbody>';
}


/** fonction auxiliaire pour la création du graphe (retourne le nombre de date où l'ELO à évolué
 * @param $pseudo
 * @return int
 */
function taille_date($pseudo){
    $db = new database();
    $sql = "SELECT date,SUM(nb_elo) FROM evo_elo WHERE pseudo = '$pseudo' GROUP BY date ORDER BY date";
    $res = $db->query($sql);
    $x = 0;
    while($row = pg_fetch_row($res)){
        $x++;
    }
    return $x;
}

/** fonction auxiliaire qui nous donne le minimum du elo d'un joueur
 * @param $pseudo
 * @return int
 */
function minElo($pseudo){
    $db = new database();
    $sql = "SELECT SUM(nb_elo) FROM evo_elo WHERE pseudo = '$pseudo' GROUP BY date ORDER BY date";
    $res = $db->query($sql);
    $tmp = 500;
    $resultat = $tmp;
    while($row = pg_fetch_row($res)){
        $tmp += $row[0];
        if ($tmp <= $resultat){
            $resultat = $tmp;
        }
    }
    return ($resultat - 10);
}

/** fonction qui créer les données du graphe
 * @param $pseudo
 */
function graph($pseudo){
    $db = new database();
    $sql = "SELECT date,SUM(nb_elo) FROM evo_elo WHERE pseudo = '$pseudo' GROUP BY date ORDER BY date";
    $sql2 = "SELECT date_ins FROM joueur WHERE pseudo = '$pseudo'";
    $res2 = $db->query($sql2);
    $row2 = pg_fetch_row($res2);
    echo "{date: '$row2[0]', a:500},";
    $res = $db->query($sql);
    $tmp = 500;
    for($i = 0; $i<taille_date($pseudo)-1; $i++){
        $row=pg_fetch_row($res);
        $tmp += $row[1];
        echo "{date: '$row[0]', a:$tmp},";
    }
    $row=pg_fetch_row($res);
    $tmp += $row[1];
    echo "{date: '$row[0]', a:$tmp}";
}

/** affiche l'ensemble des matchs d'un joueur
 * @param $pseudo
 */
function allMatch($pseudo){
    print '<tr><th>Date</th><th>Pseudo 1</th><th>Point 1</th><th>Pseudo 2</th><th>Point 2</th><th>Delta Elo</th></tr>';
    print '</thead>';
    print '<tbody>';
    $db = new database();
    $sql = "SELECT date,pseudo1,pts1,pseudo2,pts2,deltaelo,gagnant FROM rencontre WHERE pseudo1 = '$pseudo' OR pseudo2 = '$pseudo' ORDER BY date";
    $res = $db->query($sql);
    while($row = pg_fetch_row($res)){
        if($row[6] == $pseudo){
            $tmp = $row[5];
        }
        else {
            $tmp = -$row[5];
        }
        echo "<tr><td>$row[0]</td><td>$row[1]</td><td>$row[2]</td><td>$row[3]</td><td>$row[4]</td><td>$tmp</td></tr>";
    }
    echo '</tbody>';
}