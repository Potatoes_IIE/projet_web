<?php
/**
 * Created by PhpStorm.
 * User: Benjamin
 * Date: 07/05/2016
 * Time: 15:18
 * supprime un match en attente
 */
session_start();
include '../DB/dbhelp.php';
include '../html/test_admin.php';
include '../rencontre/elo.php';
include '../rencontre/match.php';
$id = $_GET['id'];


$match = new match();
$match->delMatch_attente($id);
$match->decrement($id);

header("Location: ../match_attente.php?type=1");