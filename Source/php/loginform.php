<?php
/**
 * Created by PhpStorm.
 * User: Benjamin
 * Date: 12/04/2016
 * Time: 11:38
 * vérifie les erreurs pour l'authentification
 */

//begin session
session_start();
include ("../DB/dbhelp.php");
include("../DB/form.php");

// on récupère l'email et le mot de passe entré puis on va faire les vérifications !
$mail = $_POST['email'];
$mdp = $_POST['pass'];
$token = $_POST['form_token'];

/*** first check that both the username, password and form token have been sent ***/
if(!isset($mail,$mdp,$token))
{
    header("Location: ../index.php?error=1");
}
/*** check the form token is valid ***/
elseif($token != $_SESSION['form_token'])
{
    header("Location: ../index.php?error=1");
}
/*** check the password has only alpha numeric characters ***/
elseif (ctype_alnum($mdp) != true)
{
    /*** if there is no match ***/
    header("Location: ../index.php?error=1");
}
else
{
    /*** if we are here the data is valid and we can insert it into database ***/
    $mail = filter_var($mail, FILTER_VALIDATE_EMAIL);
    $mdp = filter_var($mdp, FILTER_SANITIZE_STRING);

    /*** connect to database ***/
    $db = new form();
    $pseudo = $db->getPseudo($mail);
    if($db->next($mail,$mdp)){
        $_SESSION['user_pseudo']= $pseudo; //on l'ajoute en variable de session
        header("Location: ../accueil.php");
    }
    else {
        header("Location: ../index.php?error=1");  /**permet d'afficher l'erreur defini dans index.php**/
    }
}




