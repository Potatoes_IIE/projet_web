<?php
/**
 * Created by PhpStorm.
 * User: Benjamin
 * Date: 12/04/2016
 * Time: 11:38
 * vérification avant insertion dans la liste des inscrits
 */

//begin session
session_start();
include ("../DB/dbhelp.php");
include("../DB/tour.php");


$tournois = new tour();
if(!$tournois->testJoueur($_SESSION['user_pseudo'])){
    header("Location: ../tournoi.php");
}
elseif ($tournois->taille() >= $_SESSION['tournoi_taille']){
    header("Location: ../tournoi.php");
}
else {
    $tournois->addJoueur($_SESSION['user_pseudo'],$_SESSION['user_elo']);
    header("Location: ../tournoi.php?ins=1");
}



