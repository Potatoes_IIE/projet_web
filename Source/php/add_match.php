<?php
/**
 * Created by PhpStorm.
 * User: Benjamin
 * Date: 07/05/2016
 * Time: 15:57
 * ajoute un match en attente dans la table rencontre
 */

session_start();
include '../DB/dbhelp.php';
include '../rencontre/elo.php';
include '../rencontre/match.php';

$id = $_GET['id'];

$match = new match();

$match->addMatch($id);
$match->delMatch_attente($id);
$match->decrement($id);
$match->updateElo();


header("Location: ../match_attente.php?type=2");