<?php
/**
 * Created by PhpStorm.
 * User: Benjamin
 * Date: 09/04/2016
 * Time: 17:37
 */

/**Fichier pour sécuriser une faille**/

/*** begin our session ***/
session_start();

/*** set a form token ***/
$form_token = md5( uniqid('auth', true) );

/*** set the session form token ***/
$_SESSION['form_token'] = $form_token;
?>