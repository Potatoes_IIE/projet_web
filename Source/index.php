<?php if(isset($_GET['error'])){$error = $_GET['error'];};
include 'DB/dbhelp.php';
include 'rencontre/elo.php';
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>BabIIE</title>
    <link rel="stylesheet" type="text/css" href="styles/style.css"/>
    <link rel="shortcut icon" type="image/png" href="images/ico2.png"/>
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <?php include('security/token.php'); ?>
</head>
<header>
<div class="center">
<h1>Bab<span>IIE</span></h1>
    <br><div class='error' style="text-align: center;"><?php if(isset($error)){if($error==3){echo "Tu doit être connecté pour accéder à cette page !";} elseif($error==4){echo "Page introuvable !";}} ?></div>
</header>
<body>
<div class="form">
<ul class="tab-gr">
<li><a href="#login" class="<?php if(isset($error)){if($error==1){echo "active";}elseif($error==2){} else{echo "active";}} else echo"active"; ?>">Se connecter</a></li>
<li><a href="#signup" class="<?php if(isset($error)){if($error==2){echo "active";}elseif($error==1){} else{echo "";}} ?>">S'enregistrer</a></li>
</ul>
<div id ="login" class="<?php if(isset($error)){if($error==1){echo "show";}elseif($error==2){echo "hide";} else{echo "show";}} else echo"show"; ?>">
<br/>
<h1>CONNECTE TOI</h1>
<form action="php/loginform.php" method="post">
    <div class='error'><?php if(isset($error)){if($error==1){echo "Email ou login incorrect !";}} ?></div><br>
	<input type="email" name="email" class="" id="email" placeholder="Email" maxlength="40" required />
    <input type="hidden" name="form_token" value="<?php echo $form_token; ?>" />
    <input type="password" name="pass" pattern=".{4,}" placeholder="Password" maxlength=20 required title="Minimum 4 caractères" />
    <input type="submit" name="next" class="action-button" value="Connecte toi" />

</form>
</div>
<div id="signup" class="<?php if(isset($error)){if($error==2){echo "show";}elseif($error==1){echo "hide";} else{echo "hide";}} else echo"hide"; ?>">
<br/>
<h1>CREER TON COMPTE</h1>
<form action="php/signupform.php" method="post">
    <div class='error'><?php if(isset($error)){if($error==2){echo "Email ou Pseudo déjà existant !";}} ?></div><br>
	<input type="email" name="email" class="" id="email2" placeholder="Email" maxlength=40 required/>
    <input type="hidden" name="form_token" value="<?php echo $form_token; ?>" />
    <input type="password" name="pass" class="inline" pattern=".{4,}" placeholder="Password" maxlength=20 id="pass" required title="Minimum 4 caractères" />
    <input type="password" name="cpass" class="inline" pattern=".{4,}" placeholder="Confirm Password" maxlength=20 id="cpass" required title="Minimum 4 caractères" />
    <br><br>
    <input type="text" name="prenom" class="inline" placeholder="Prénom" maxlength=40 required/>
    <input type="text" name="nom" class="inline" placeholder="Nom" maxlength=40 required/>
    <input type="text" name="pseudo" placeholder="Pseudo" maxlength=40 required/>
    <input type="submit" name="next" class="action-button" value="C'est parti !" />
</form>
</div>
</div>
<div>
<?php
    include("DB/form.php");
    include("php/classement_index.php");
    consult();
?>
</div>
<script src="script/jquery.min.js"></script> <!-- Script à la fin pour que la page se charge + rapidement même si -->
<script src="script/auth.js"></script> <!-- ils sont pas imposant, tout comme la page -->
</body>
</html>
