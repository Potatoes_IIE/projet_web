<?php
/**
 * Created by PhpStorm.
 * User: Benjamin
 * Date: 24/04/2016
 * Time: 15:52
 * vérifie si un joueur est loggé
 */


if(!isset($_SESSION['user_pseudo']))
{
    header("Location: ../index.php?error=3") or header("Location: index.php?error=3"); //erreur pas loggé
}