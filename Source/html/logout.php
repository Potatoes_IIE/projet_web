<?php
/**
 * Created by PhpStorm.
 * User: Benjamin
 * Date: 24/04/2016
 * Time: 15:45
 * fichier de déconnection du site
 */

// Begin the session
session_start();

// Unset all of the session variables.
session_unset();

// Destroy the session.
session_destroy();

// Return to main menu
header("Location: ../index.php");