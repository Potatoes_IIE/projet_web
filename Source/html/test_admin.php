<?php
/**
 * Created by PhpStorm.
 * User: Benjamin
 * Date: 05/05/2016
 * Time: 15:44
 * verifie si un joueur est admin ou pas avant d'accéder à une page
 */

if(!$_SESSION['user_admin'])
{
    header("Location: ../accueil.php?type=2") or header("Location: accueil.php?type=2"); //erreur pas loggé
}