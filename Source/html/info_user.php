<?php
/** usage: ensemble des informations d'un utilisateur
 * Created by PhpStorm.
 * User: Benjamin
 * Date: 24/04/2016
 * Time: 19:37
 */


$pseudo = $_SESSION['user_pseudo'];
$db = new database();

/**On insère l'ensemble des données utile pour l'affichage du profil dans des variables de session **/
$_SESSION['user_nom'] = $db->getNom($pseudo);
$_SESSION['user_email'] = $db->getEmail($pseudo);
$_SESSION['user_prenom'] = $db->getPrenom($pseudo);
$_SESSION['user_admin'] = $db->getAdmin($pseudo);
$_SESSION['user_date_ins'] = $db->getDate($pseudo);
$_SESSION['user_elo'] = $db->getElo($pseudo);
$_SESSION['user_classement'] = $db->getClassement($pseudo);

